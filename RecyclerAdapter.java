package com.example.ritwikgupta.mobile_list;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class Recycleradapter extends RecyclerView.Adapter<Recycleradapter.ViewHolder> {
    Context context;
    ArrayList<Smartphone> arrayList;
    public Recycleradapter(Context context, ArrayList<Smartphone> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }
    @NonNull
    @Override
    public Recycleradapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.custom,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Recycleradapter.ViewHolder holder, int position) {
        Smartphone s=arrayList.get(position);
        holder.img1.setImageResource(s.image);
        holder.txt1.setText(s.brand);
        holder.txt2.setText(s.model);
        holder.txt3.setText(s.ram);
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img1;
        TextView txt1,txt2,txt3;
        public ViewHolder(View itemView) {
            super(itemView);
            img1=itemView.findViewById(R.id.img1);
            txt1=itemView.findViewById(R.id.txt1);
            txt2=itemView.findViewById(R.id.txt2);
            txt3=itemView.findViewById(R.id.txt3);

        }
    }
}
