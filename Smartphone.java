package com.example.ritwikgupta.mobile_list;

public class Smartphone {
    int image;

    public Smartphone(int image, String brand, String model, String ram) {
        this.image = image;
        this.brand = brand;
        this.model = model;
        this.ram = ram;
    }

    public String getBrand() {
        return brand;
    }

    String brand;

    public String getModel() {
        return model;
    }

    String model;

    public String getRam() {
        return ram;
    }

    String ram;

}
