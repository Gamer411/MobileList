package com.example.ritwikgupta.mobile_list;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity{
RecyclerView rc1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }
    void init() {
        rc1 = findViewById(R.id.rc1);
        Recycleradapter recycleradapter=new Recycleradapter(this,initdata());
        RecyclerView.LayoutManager layoutManager=new LinearLayoutManager(this,1,false);
        rc1.setLayoutManager(layoutManager);
        rc1.setAdapter(recycleradapter);
    }

    private ArrayList initdata() {
        Smartphone s = new Smartphone(R.mipmap.oneplus, "OnePlus", "6", "64Gb");
        ArrayList arrayList = new ArrayList();
        arrayList.add(s);
        Smartphone s1 = new Smartphone(R.mipmap.iphone, "Apple", "X", "128Gb");
        arrayList.add(s1);
        Smartphone s2 = new Smartphone(R.mipmap.samsung, "Samsung", "S9", "128Gb");
        arrayList.add(s2);
        Smartphone s3= new Smartphone(R.mipmap.pixel2,"Google","Pixel","64Gb");
        arrayList.add(s3);
        Smartphone s4= new Smartphone(R.mipmap.honor7,"Honor","7X","64Gb");
        arrayList.add(s4);
        return arrayList;
    }
